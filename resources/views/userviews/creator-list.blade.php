@extends('layouts.app')
@section('content')
	<h1 class="text-center py-5">Creators</h1>

	<div class="container">
		<div class="row">
			
			@foreach($creators as $creator)
			<div class="col-lg-6 offset-lg-2 my-5">
				<div class="card">
					<div class="card-head">
						<img src="" alt="broken img">
						<h4 class="text-center">{{ $creator->name }}</h4>
					</div>
					<hr>
					<div class="card-body">
						<p>{{ $creator->contact_number }}</p>
						<p>{{ $creator->email }}</p>
						<p>{{ $creator->address }}</p>
						<p>{{ $creator->website }}</p>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
@endsection