@extends('layouts.app')
@section('content')
	
	<h1 class="py-5 text-center">Catalog</h1>
	
	<div class="row">
		<div class="col-lg-2">
			
		</div>
		<div class="col-lg-10">
			<div class="container">
				<div class="row">
					@foreach($items as $item)
						<div class="col-lg-4">
							<div class="card">
								<div class="card-head">
									<img src="" alt="broken img">
									<h4 class="text-center">{{ $item->name }}</h4>
								</div>
								<hr>
								<div class="card-body">
									<p>{{ $item->category->name }}</p>
									<p>{{ $item->description }}</p>
									<p>{{ $item->creator->name }}</p>
									<p>{{ $item->status->name }}</p>
								</div>
								<div class="card-footer">
									<a href="/editdress/{{ $item->id }}" class="btn btn-info btn-block">Edit</a>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	
	

@endsection