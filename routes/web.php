<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//route for catalog page
Route::get('/catalog', 'ItemController@index');

//route for edit(user)
Route::get('/editdress/{id}', 'ItemController@editDress');
Route::patch('/editdress/{id}', 'ItemController@update');

Route::get('/creatorlist', 'ItemController@creatorList');

