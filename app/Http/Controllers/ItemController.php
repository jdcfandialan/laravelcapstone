<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use \App\Status;
use \App\Creator;

class ItemController extends Controller
{
    public function index(){

    	$items = Item::all();

    	return view('userviews.catalog', compact('items'));

    }

    public function creatorList(){

    	$creators = Creator::get();

    	return view('userviews.creator-list', compact('creators'));

    }

    public function editDress($id){

    	$item = Item::find($id);

    	$categories = Category::all();

    	$statuses = Status::all();

    	$creators = Creator::all();

    	return view('userviews.editdress', compact('item','categories', 'statuses','creators'));
    }

    public function update($id, Request $req){

    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"category_id" => "required",
    		"status_id" => "required",
    		"creator_id" => "required"
    	);


    	$this->validate($req, $rules);

    	$editItem = Item::find($id);

    	$editItem->name = $req->name;
    	$editItem->description = $req->description;
    	$editItem->status_id = $req->status_id;
    	$editItem->category_id = $req->category_id;
    	$editItem->creator_id = $req->creator_id;

    	$editItem->save();

    	return redirect('/catalog');
    }
}
