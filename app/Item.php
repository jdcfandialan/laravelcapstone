<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Creator;
use \App\Category;
use \App\Status;

class Item extends Model
{
    public function creator(){
    	return $this->belongsTo('\App\Creator');
    }

    public function category(){
    	return $this->belongsTo('\App\Category');
    }

    public function status(){
    	return $this->belongsTo('\App\Status');
    }
}
